'use strict';

/**
 * @ngdoc function
 * @name runApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the runApp
 */
angular.module('runApp')
  .controller('GameCtrl', function (authUser, $location, $scope, sessionControl,$timeout,$http,toastr) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    var vm = this;
    vm.loginForm = {
      distancia:'',
      tipo:'',
      descripcion:'',
      time:'',
      user_id:''
    }
    
    vm.loading = false;
    vm.email_login = authUser.getEmail();
    vm.photoprofile = authUser.getPhoto();
    vm.nameprofile = authUser.getName();
    vm.user_id = authUser.getId();
    vm.listasessiones = {};

    vm.isLogin = authUser.isLoggedIn();
    vm.loginForm.tipo = "Long stride";

    $scope.$watch(function(){
    	return authUser.isLoggedIn();
    },function(newVal){
    	if(typeof newVal !== 'undefined'){
    		vm.isLogin = authUser.isLoggedIn();
    	}
    });

    vm.user= {
    	email: sessionControl.get('email')
    }

    vm.logout = function(){
    	authUser.logout();
    }

    

    vm.inicio=0;
    vm.timeout=0;
    vm.result = "00:00:00";
    vm.btn_crono = "Start";
    vm.empezarDetener = function()
    {
      if(vm.timeout==0)
      {
        vm.btn_crono = "Stop";
        // empezar el cronometro
   
        //elemento.value="Detener";
   
        // Obtenemos el valor actual
        vm.inicio=vm.vuelta=new Date().getTime();
   
        // iniciamos el proceso
        vm.funcionando();
        
      }else{
        vm.btn_crono = "Start";
        // detemer el cronometro
   
        //elemento.value="Empezar";
        $timeout.cancel(vm.timeout);
        vm.timeout=0;

      }
    }
   
    vm.funcionando = function()
    {
      // obteneos la fecha actual
      var actual = new Date().getTime();
   
      // obtenemos la diferencia entre la fecha actual y la de inicio
      var diff=new Date(actual-vm.inicio);
   
      // mostramos la diferencia entre la fecha actual y la inicial
      var result = vm.LeadingZero(diff.getUTCHours())+":"+vm.LeadingZero(diff.getUTCMinutes())+":"+vm.LeadingZero(diff.getUTCSeconds());  
      vm.result = result;
   
      
      vm.timeout = $timeout(vm.funcionando, 1000);
    }
   
    vm.LeadingZero = function (Time) {
      return (Time < 10) ? "0" + Time : + Time;
    }

    
    vm.savesession = function(){
      vm.loginForm.time = vm.result;
      vm.loginForm.user_id = vm.user_id;   
      console.log("AJA",vm.loginForm);
      if(vm.loginForm.distancia!=""&&vm.loginForm.tipo!=""&&vm.loginForm.descripcion!=""&&vm.loginForm.time!=""){
          vm.loading = true;

          var requestGuardarSession = {
              method: 'POST',
              url: 'http://www.masterd.com.ve/qapi/public/sessionsave',
              //url: 'http://pruebas/qapi/public/sessionsave',
              data: {data:vm.loginForm},
              headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
          }
         // console.log(requestGuardarSession.data);
          var token = localStorage.getItem('Token');
              //$http.defaults.headers.common.Authorization = token;
              $http(requestGuardarSession).then(function(response){
                  //localStorage.setItem('Token', response.headers()['authorization']);
                  vm.loading = false;
                  vm.loginForm = {
                    distancia:'',
                    tipo:'Long stride',
                    descripcion:'',
                    user_id:''
                  }
                  toastr.success("Session saved",'Message');
                  //console.log("POST TRAJO:",response.data[0]);
                  //console.log("WHAT",response.headers()['authorization']);

                  vm.listasession();
              },function(err){
                  vm.loading = false;
                  toastr.error("Error, Try again please",'Error');
                  console.log(err);
          })
      }

      
    }



    vm.listasession = function(){
      vm.loading = false;
      var requestBuscar = {
          method: 'POST',
          url: 'http://www.masterd.com.ve/qapi/public/sessionslist',
          //url: 'http://pruebas/qapi/public/sessionslist',
          data: {data:vm.user_id},
          headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
      }
      //console.log(requestBuscar.data);
      var token = localStorage.getItem('Token');
          //$http.defaults.headers.common.Authorization = token;
          $http(requestBuscar).then(function(response){
              //localStorage.setItem('Token', response.headers()['authorization']);
              vm.listasessiones = response.data;
              vm.loading = false;
          },function(err){
              toastr.error("Error in list session, Try again please",'Error');
              console.log(err);
              vm.loading = false;
      })
    }

    vm.listasession();
  });
