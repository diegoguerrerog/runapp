'use strict';

angular.module('runApp')
	.controller('LoginCtrl',function(authUser,$http,toastr, $scope){		
		var vm = this;
		
		vm.loginForm = {
			email:'',
			password:''
		}
		
		vm.loading = false;
		vm.login = function(){
			if ((typeof vm.loginForm.email != 'undefined')&&(typeof vm.loginForm.password != 'undefined')){
				$("#btn_entrar").attr('disabled','disabled');

				if ((typeof vm.loginForm.email != 'undefined')&&(typeof vm.loginForm.password != 'undefined')){			
					vm.loading = true;
					authUser.loginApi(vm.loginForm);
					$("#btn_entrar").removeAttr('disabled');
					vm.loading = false;
				}
				//console.log("va ",vm.loginForm);
			}
		}
		vm.data = {};
		vm.loginFormr = {};
		vm.register = function(){
			
			$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
			if ((typeof vm.loginFormr.emailr != 'undefined')&&(typeof vm.loginFormr.passr != 'undefined')&&(typeof vm.loginFormr.passrc != 'undefined')){			
				if(vm.loginFormr.passr == vm.loginFormr.passrc){
					vm.loading = true;
					$("#btn_registrar").attr('disabled','disabled');
					vm.data.email = vm.loginFormr.emailr;
					vm.data.password = vm.loginFormr.passr;
					vm.data.name = "";
					$http({
				        url: 'http://www.masterd.com.ve/qapi/public/register',
				        //url: 'http://pruebas/qapi/public/register',
				        method: "POST",
				        data: vm.data,
				        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
				    })
				    .then(function(response) {
				    	vm.loading = false;
				    	$("#btn_registrar").removeAttr('disabled');
				    	if(response.data == "Email is already registered"){
				    		toastr.error(response.data,'Error');
				    	}else if(response.data == "It was successfully registered"){
				    		toastr.success(response.data,'Mensaje');
				    		vm.loginForm.email = vm.data.email;
				    		vm.loginFormr = {};
				    		toastr.success("Sign in to train",'Mensaje');
				    	}
				    	
				    }, 
				    function(response) { // optional
				        toastr.error(response.data,'Error');
				        vm.loading = false;
				    });
				}else{
					toastr.error('Passwords do not match','Error');
				}
				//authUser.loginApi(vm.loginFormr);
				//console.log("genial diego :D");
			}
			//console.log("va ",vm.data);
		}
});