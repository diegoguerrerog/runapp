'use strict';

/**
 * @ngdoc function
 * @name runApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the runApp
 */
angular.module('runApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
