'use strict';

/**
 * @ngdoc function
 * @name runApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the runApp
 */
angular.module('runApp')
  .controller('ProfileCtrl', function ($scope,$http,toastr,$rootScope,authUser,$base64,sessionControl) {
    var vm = this;
    vm.loginForm = {
		email:'',
		firstname:'',
		lastname:'',
		age:'',
		gender:'',
		file:''
	}
	vm.fileimage = {};
	vm.loading = false;
	vm.email_login = authUser.getEmail();
    vm.photoprofile = authUser.getPhoto();
    vm.nameprofile = authUser.getName();
	vm.loginForm.email = vm.email_login;
    //console.log(vm.nameprofile);
	//console.log("inicio sesion con:",vm.email_login);
	vm.updateprofile = function(){

        if(vm.loginForm.email!=""&&vm.loginForm.firstname!=""&&vm.loginForm.lastname!=""&&vm.loginForm.age!=""&&vm.loginForm.gender!=""&&vm.loginForm.file!=""){
            var requestGuardar = {
                method: 'PUT',                 
                url: 'http://www.masterd.com.ve/qapi/public/profile',
                //url: 'http://pruebas/qapi/public/profile',
                data: {data:vm.loginForm},
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            }
            //console.log(requestGuardar.data);
            vm.loading = true;
            //var token = localStorage.getItem('Token');
                $http.defaults.headers.common.Authorization = token;
                $http(requestGuardar).then(function(response){
                    //localStorage.setItem('Token', response.headers()['authorization']);
                    vm.loading = false;
                    //console.log(response);
                    //console.log("WHAT",response.headers()['authorization']);
                    toastr.success('Data saved','Message');
                    authUser.setPhoto(vm.loginForm.file);
                    vm.photoprofile = vm.loginForm.file;
                },function(err){
                    vm.loading = false;
                    toastr.error('Data unsaved, try again please','Error');
                    console.log(err);
            })
        } 
	}



	var requestBuscar = {
        method: 'POST',
        url: 'http://www.masterd.com.ve/qapi/public/profilesearch',
        //url: 'http://pruebas/qapi/public/profilesearch',
        data: {data:vm.loginForm},
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    }
    //console.log(requestBuscar.data);
    vm.loading = true;
    var token = localStorage.getItem('Token');
        //$http.defaults.headers.common.Authorization = token;
        $http(requestBuscar).then(function(response){
            //localStorage.setItem('Token', response.headers()['authorization']);
            vm.loginForm.firstname = response.data[0].name;
            vm.loginForm.lastname = response.data[0].lastname;
            vm.loginForm.age = response.data[0].age;
            vm.loginForm.gender = response.data[0].gender;
            vm.loginForm.file = response.data[0].photo;
            vm.loading = false;
        },function(err){
           vm.loading = false;
            console.log(err);
    })



  $scope.imageStrings = [];
  $scope.processFiles = function(files){
    angular.forEach(files, function(flowFile, i){
       var fileReader = new FileReader();
          fileReader.onload = function (event) {
            var uri = event.target.result;
              $scope.imageStrings[i] = uri; 
              //console.log("foto:",$scope.imageStrings[i]);
              vm.loginForm.file = $scope.imageStrings[i];    
          };
          fileReader.readAsDataURL(flowFile.file);
    });
  };

  vm.isLogin = authUser.isLoggedIn();


    $scope.$watch(function(){
        return authUser.isLoggedIn();
    },function(newVal){
        if(typeof newVal !== 'undefined'){
            vm.isLogin = authUser.isLoggedIn();
        }
    });

    vm.user= {
        email: sessionControl.get('email')
    }

    vm.logout = function(){
        authUser.logout();
    }


});
