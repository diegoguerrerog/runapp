'use strict';
angular.module('authService',[])
	.factory('sessionControl',function(){
		return {
			get:function(key){
				return sessionStorage.getItem(key);
			},
			set:function(key,val){
				return sessionStorage.setItem(key,val); 
			},
			unset:function(key){
				return sessionStorage.removeItem(key);
			}
		};
	})
	.factory('authUser',function($auth,sessionControl,toastr,$location){

		var cacheSession = function(email,photo,name,id_user){
			sessionControl.set('userIsLogin',true);
			sessionControl.set('email',email);
			sessionControl.set('photo',photo);
			sessionControl.set('name',name);
			sessionControl.set('id_user',id_user);
		}

		var unCacheSession = function(){
			sessionControl.unset('userIsLogin');
			sessionControl.unset('email');
			sessionControl.unset('photo');
			sessionControl.unset('name');
			sessionControl.unset('id_user');
		}

		var login = function (loginForm){
			$auth.login(loginForm).then(
				function (response){
					if(typeof response.data.error !== 'undefined'){
			    		toastr.error('The data is incorrect','Error');
			    	}else{
			    		cacheSession(response.data.user.email,response.data.user.photo,response.data.user.name,response.data.user.id);			    		   		
						$location.path('/juego');
						toastr.success('Session started successfully','Mensaje');
			    	}
				},
				function (error){
					unCacheSession();
					toastr.error(error.data.error,'Error');
					console.log(error);
				}
			);
		};

		return {
			loginApi: function (loginForm){
				login(loginForm);
			},
			logout:function(){
				$auth.logout();
				unCacheSession();
				toastr.success('Session closed successfully','Mensaje');
				$location.path('/');
			},
			isLoggedIn:function(){
				return sessionControl.get('userIsLogin') !== null;
			},
			getEmail:function(){
				return sessionControl.get('email');
			},
			getPhoto:function(){
				return sessionControl.get('photo');
			},
			getName:function(){
				return sessionControl.get('name');
			},
			setPhoto:function(datphoto){
				sessionControl.set('photo',datphoto);
			}
			,
			getId:function(){
				return sessionControl.get('id_user');
			},

		}

	});
	