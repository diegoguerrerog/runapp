'use strict';

/**
 * @ngdoc overview
 * @name runApp
 * @description
 * # runApp
 *
 * Main module of the application.
 */
angular
  .module('runApp', [
    'authService',
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'satellizer',
    'toastr',
    'base64',
    'flow'
  ])
  .config(function ($routeProvider,$authProvider) {
    $authProvider.loginUrl = 'http://www.masterd.com.ve/qapi/public/auth_login';
    //$authProvider.loginUrl = 'http://pruebas/qapi/public/auth_login';
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/juego', {
        templateUrl: 'views/game.html',
        controller: 'GameCtrl',
        controllerAs: 'about'
      })
      .when('/login',{
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/profile',{
        templateUrl: 'views/profile.html',
        controller: 'ProfileCtrl',
        controllerAs: 'profile'
      })
      .otherwise({
        redirectTo: '/'
      });
      
  }).config(function ($httpProvider) {
  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
  $httpProvider.defaults.headers.put = {};
  $httpProvider.defaults.headers.patch = {};
}).config(function ($httpProvider) {
      $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded';
      $httpProvider.defaults.headers.post['Content-Type'] =  'application/x-www-form-urlencoded';
  }).run(function($rootScope,$location, authUser, toastr){
    var rutasPrivadas = ['/juego','/profile'];
    $rootScope.$on('$routeChangeStart',function(){
      if(($.inArray($location.path(),rutasPrivadas)!==-1) && !authUser.isLoggedIn()){
        toastr.error('Debe iniciar sesion para poder entrar al juego','Mensaje');
        $location.path('/login');
      }
    });
  });
